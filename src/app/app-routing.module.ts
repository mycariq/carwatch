import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './service/auth-guard.service';

const routes: Routes = [
  { path: '', redirectTo: 'launch', pathMatch: 'full' },
  { path: '', loadChildren: './menu/menu.module#MenuPageModule', canActivate: [AuthGuardService] },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'verification', loadChildren: './login/verification/verification.module#VerificationPageModule' },
  { path: 'launch', loadChildren: './login/cqlaunch/cqlaunch.module#CqlaunchPageModule', canActivate: [AuthGuardService] },
  { path: 'profile', loadChildren: './menu/profile/profile.module#ProfilePageModule', canActivate: [AuthGuardService] },
  { path: 'home', loadChildren: './home/home.module#HomePageModule', canActivate: [AuthGuardService] },

  //Not required
  // { path: 'menu', loadChildren: './menu/menu.module#MenuPageModule' },
  // { path: 'vehicles', loadChildren: './menu/vehicles/vehicles.module#VehiclesPageModule' },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
