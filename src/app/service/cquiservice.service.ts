import { Injectable } from '@angular/core';
import { LoadingController, AlertController, ToastController, Platform } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class CquiserviceService {

  constructor(public loadingController: LoadingController,
              public alertController: AlertController,
              public toastController: ToastController,
              public platform: Platform) {
  }

  // present Alert message
  async presentAlertMessage(headers: string, messages: string, subheader: string = null) {
    const alert = await this.alertController.create({
      header: headers,
      subHeader: subheader,
      message: messages,
      buttons: ['OK']
    });

    await alert.present();
  }

  async presentAlertMessageMuiltipleButtons(headers: string, messages: string, subheader: string = null, buttonsList: any) {
    const alert = await this.alertController.create({
      header: headers,
      subHeader: subheader,
      message: messages,
      buttons: buttonsList
    });

    await alert.present();
  }

  // present Toast message
  async presentToastMessage(messages: string, durations: number = 2000) {
    const toast = await this.toastController.create({
      message: messages,
      duration: durations
    });

    toast.present();
  }

  async presentTransparentToastMessage() {
    const toast = await this.toastController.create({
      duration: 0.5,
      translucent: true,
      color: 'transparent'
    });

    toast.present();
  }

  // present loading controller
  async presentLoading(messages: string = 'Loading...') {
    const loadingoptions = {
      message: messages
    };

    return await this.loadingController.create(loadingoptions);
  }

  // dismiss loader
  async dismissLoading() {
    return await this.loadingController ? this.loadingController.dismiss() : '';
  }
}
