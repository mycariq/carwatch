import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot } from '@angular/router';
import { Utils } from '../helper/utils';
import { DataService } from './data.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {

  constructor(private router: Router, private dataservice: DataService) { }


  canActivate(route: ActivatedRouteSnapshot): boolean {

    this.dataservice.getFromCache(Utils.CURRENT_USER).then(credentials => {
      if (!credentials) {
        this.router.navigate(['login']);
        return false;
      }
    });
    return true;
  }
}
