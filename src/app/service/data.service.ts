import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CQAPI } from '../toolkit/cqapi';
import { apiJSON } from '../api.js';
import { CQLogin } from '../toolkit/cqlogin';
import { CQServer } from '../toolkit/cqserver';
import { Utils } from '../helper/utils';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { User } from '../interface/user';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private storage: NativeStorage;
  current_login: CQLogin;
  current_user: User;

  constructor(private http: HttpClient) {
    this.storage = new NativeStorage();
  }

   /**
   * call
   */
  public call(apiKey: string, convertor: any, actorForSuccess?: any, actorForFailure?: any, login?: CQLogin, server?: CQServer) {
    // GET api json from a key, login and server
    let apiRequestJSON = apiJSON[apiKey];

    let CURRENT_USER = this.getLogin(login, apiRequestJSON.role);
    let CURRENT_SERVER = this.getServer(server, apiRequestJSON.server);

    // Call api from the server
    let api = new CQAPI(apiRequestJSON, CURRENT_USER, CURRENT_SERVER, convertor);

    this.request(api.method(), api.url(), api.inputParams(), api.headers())
      .subscribe(response => actorForSuccess(response), error => actorForFailure(error));
  }


  // save values into local/ native / sqllite / or any other storage
  public saveIntoCache(key: string, value: any) {
    this.storage.setItem(key, value);
  }

  // get value from storage media
  public getFromCache(key: string): Promise<any> {
    return this.storage.getItem(key);
  }

  // Clear all data store in native storage
  public clearCache() {
    this.storage.clear();
  }

 
  // Call api through http.request methiod
  private request(method: string, url: string, params?: any, authHeaders?: any): Observable<any> {
    let httpOptions = {
      body: params,
      headers: authHeaders
    };
    return this.http.request(method, url, httpOptions);
  }

  /**
   * getLogin
   */
  private getLogin(login: CQLogin, role: String): CQLogin {

    // Based on current user is present or not, or input login or apiJson login, set login
    if (login !== undefined) {
      return login;
    }

    if (role !== undefined) {
      switch (role) {
        case 'NEW_USER':
          return CQLogin.NEW_USER;
        default:
          break;
      }
    }

    // If login user is not given, Send current logged in user.
    return this.current_login;
  }

  /**
   * getServer
   */
  private getServer(server: CQServer, serverName: String): CQServer {

    // Based on current server is present or not or input server or apiJson server, set server
    if (server !== undefined) {
      return server;
    }

    if (serverName !== undefined) {
      switch (serverName) {
        case 'GOOGLE':
          return CQServer.GOOGLE;
          break;
        default:
          break;
      }
    }
    return CQServer.DEFAULT;
  }

}
