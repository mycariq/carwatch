import { InAppBrowserOptions, InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {
  GoogleMap,
  GoogleMaps,
  GoogleMapOptions,
  Environment,
  GoogleMapsEvent,
  CameraPosition,
  ILatLng,
  MarkerCluster,
  LatLng
} from '@ionic-native/google-maps/ngx';
import { CQBase } from '../toolkit/cqbase';
import { Router } from '@angular/router';
import { DataService } from '../service/data.service';
import { CquiserviceService } from '../service/cquiservice.service';
import * as moment from 'moment';
import { DrawerState } from '../modules/ion-bottom-drawer/drawer-state';
import { IonInput } from '@ionic/angular';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { googleMapUtils } from '../helper/googleMapUtils';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { Utils } from '../helper/utils';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage extends CQBase implements OnInit {

  @ViewChild('dstate') dstate: IonInput;

  vehicles: Array<any>;

  // map variable
  map: GoogleMap;
  markerCluster: MarkerCluster;

  // Drawer line
  shouldBounce = true;
  disableDrag = true;
  dockedHeight = 60;
  distanceTop = 300;
  drawerState = DrawerState.Bottom;
  minimumHeight = 0;

  //Currently selected vehicle
  selectedVehicle: any = {
    registrationNumber: '',
    make: '',
    model: ''
  };
  search_vehicle: string;
  markers: Array<any> = [];
  bounds: any = [];
  selectedVehicleLocationUrl: any;

  constructor(public router: Router, public dataservice: DataService,
    public uiService: CquiserviceService, public inappbrowser: InAppBrowser,
    public callNumber: CallNumber, public keyboard: Keyboard) {
    super(router, dataservice, uiService);
  }

  ngOnInit(): void {

    // Initialize map
    this.loadMap();

    // Get active vehicle list of observer
    this.getVehicles();

  }

  /**
   * loadMap
   */
  public loadMap() {
    // Set envirnoment key
    Environment.setEnv({
      'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyB90ZNJ0iouzaWKv5wVg3wH2IGb8rh9EvI',
      'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyB90ZNJ0iouzaWKv5wVg3wH2IGb8rh9EvI'
    });

    const options: GoogleMapOptions = {
      controls: {
        compass: true,
        myLocation: false,
        myLocationButton: false
      }
    };

    this.map = GoogleMaps.create('map_canvas', options);

    this.map.on(GoogleMapsEvent.MAP_CLICK).subscribe(() => {
      // Close drawer bar
      this.setDrawerOff();
    })
  }

  /**
   * getVehicles
   */
  public getVehicles() {

    //Call get Obserable vehicles of given observer
    this.call('getObservation', {}, response => {

      // Return only active vehicles
      this.vehicles = response.filter(row => {
        return moment(row.observeTill).isAfter(moment().valueOf());
      });


      // Set marker of every active vehicle 
      for (let i = 0; i < this.vehicles.length; i++) {
        this.setMarker(this.vehicles[i])
      }

      this.addCluster();

    }, fail => {
      this.presentAlertMessage('Alert', 'No vehicles to track!!');
    });

  }

  /**
   * setMarker  
   */
  public setMarker(vehicle: any) {

    const marker = this.map.addMarkerSync(
      {
        title: vehicle.carDetail.registrationNumber,
        position: { lat: vehicle.carDetail.latitude, lng: vehicle.carDetail.longitude },
        icon: Utils.setMarker(vehicle.carDetail.vehicleType)
      });

    marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {

      // Set currently selected vehicle on marker click event
      this.selectedVehicle = vehicle.carDetail;
      this.selectedVehicleLocationUrl = vehicle.locationShare.url;

      // Show marker title on click
      marker.showInfoWindow();

      if (this.drawerState) {
        // Close drawer bar
        this.setDrawerOff();
        // Open drawer bar
        this.setDrawerOn()
      } else {
        // Open drawer bar
        this.setDrawerOn()
      }
    });

    const position: CameraPosition<ILatLng> = {
      target: marker.getPosition(),
      zoom: 5
    };
    this.bounds.push(new LatLng(vehicle.carDetail.latitude, vehicle.carDetail.longitude));
    this.markers.push(marker);
    // this.map.animateCamera(position);
    this.map.moveCamera(position);

  }

  addCluster() {

    if (!this.markers) {
      return;
    }

    this.markerCluster = this.map.addMarkerClusterSync({
      markers: this.markers,
      icons: [
        {
          min: 2,
          url: 'assets/imgs/cluster.png',
        }
      ]
    });

    googleMapUtils.animateCameraWithBounds(this.map, this.bounds);

    // }, 10);
    // setTimeout(() => {
    //   googleMapUtils.animateCameraWithBounds(this.map, this.bounds);
    // }, 10);
  }

  // Set Drawer Off
  private setDrawerOff() {
    this.drawerState = DrawerState.Bottom;
    this.dstate.value = DrawerState.Bottom.toString();
    this.dstate.setFocus();
  }

  // Set Drawer On
  private setDrawerOn() {
    this.drawerState = DrawerState.Docked;
    this.dstate.value = DrawerState.Docked.toString();
    this.dstate.setFocus();
  }

  /**
   * trackVehicle
   */
  public trackLocation(url: string) {
    this.loadInappBrowser(url);
  }


  // loadIn app Browser with given URL string
  public loadInappBrowser(URLString: any) {
    const options: InAppBrowserOptions = {
      location: 'no',
      clearcache: 'yes',
      zoom: 'no',
      toolbar: 'yes',
      closebuttoncaption: 'back'
    };

    // To allow mix content webpage followed belowed url
    // https://stackoverflow.com/questions/42119836/how-to-load-third-party-non-secure-images-in-cordova-webview
    // // _self, _blank, _system
    const browser = this.inappbrowser.create(URLString, '_blank', options);
  }

  /**
   * doRefresh
   */
  public doRefresh(event) {
    this.ngOnInit();
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }


  /**
   * trackVehicleLocation
   */
  public trackVehicleLocation() {
    this.trackLocation(this.selectedVehicleLocationUrl);
  }

  // /**
  //  * shareLocation
  //  */
  // public shareLocation() {
  //   if (this.selectedVehicle) {
  //     this.sharing.share('Track my location real-time',
  //       'Realtime Location Tracking',
  //       null,
  //       this.selectedVehicleLocationUrl).then(() => {
  //         console.log('sharing: success');
  //       })
  //       .catch(() => {
  //         console.log('sharing: failed');
  //       });
  //   }
  // }

  /**
   * callDriver
   */
  callDriver() {
    if (this.selectedVehicle) {
      if (this.selectedVehicle.contactNumber !== null) {
        if (this.callNumber.isCallSupported()) {
          this.callNumber.callNumber(this.selectedVehicle.contactNumber, true)
            .then(res => console.log('Launched dialer!', res))
            .catch(err => console.log('Error launching dialer', err));
        }
      } else {
          this.presentAlertMessage('Alert', "Call not connected");
      }
    }
  }


  /**
   * searchVehicle
   */
  public searchVehicle() {
    let vehicle, marker = undefined;
    for (let i = 0; i < this.vehicles.length; i++) {
      if (this.vehicles[i].carDetail.registrationNumber.toUpperCase().indexOf(this.search_vehicle.toUpperCase()) > -1) {

        // Set currently selected vehicle and its url
        this.selectedVehicle = this.vehicles[i].carDetail;
        this.selectedVehicleLocationUrl = this.vehicles[i].locationShare.url
        vehicle = this.vehicles[i];
        marker = this.markers[i];
        break;
      }
    }

    // If vehicle found, show on a map
    if (vehicle != undefined) {
      const position: CameraPosition<ILatLng> = {
        target: marker.getPosition(),
        zoom: 14
      };

      marker.showInfoWindow();
      this.keyboard.hide();
      // this.setDrawerOn();
      // this.map.animateCamera(position);
      this.map.moveCamera(position);
    }
    else {
      alert("vehicle not located on a map");
    }
  }
}
