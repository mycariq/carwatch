export const apiJSON = {
    loginrequest: {
        url: "Cariq/loginAccess/request",
        method: "POST",
        role: "NEW_USER",
        params: [
            "validTill",
            "otp",
            "used",
            "userName"
        ]
    },
    loginprocess_old: {
        url: "Cariq/loginAccess/process",
        method: "POST",
        role: "NEW_USER",
        params: [
            "validTill",
            "otp",
            "used",
            "userName"
        ]
    },
    loginprocess: {
        url: "Cariq/loginAccess/process/observer",
        method: "POST",
        role: "NEW_USER",
        params: [
            "validTill",
            "otp",
            "used",
            "userName"
        ]
    },
    authenticate: {
        url: "Cariq/authenticate/observer",
        method: "POST",
        role: "NEW_USER",
        params: [
            "username",
            "password"
        ]
    },
    getObservation: {
        url: "Cariq/observer/observation",
        method: "GET"
    },
    updateProfile: {
        url: "Cariq/observer/{id}",
        method: "PUT",
        params: [
            "name",
            "email",
            "cellNumber"
        ]
    }
}