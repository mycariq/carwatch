import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CqlaunchPage } from './cqlaunch.page';

const routes: Routes = [
  {
    path: '',
    component: CqlaunchPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CqlaunchPage]
})
export class CqlaunchPageModule {}
