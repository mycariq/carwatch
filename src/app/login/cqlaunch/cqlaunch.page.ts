import { Component, OnInit } from '@angular/core';
import { CQBase } from 'src/app/toolkit/cqbase';
import { Router } from '@angular/router';
import { DataService } from 'src/app/service/data.service';
import { CquiserviceService } from 'src/app/service/cquiservice.service';
import { Utils } from 'src/app/helper/utils';
import { CQLogin } from 'src/app/toolkit/cqlogin';

@Component({
  selector: 'app-cqlaunch',
  templateUrl: './cqlaunch.page.html',
  styleUrls: ['./cqlaunch.page.scss'],
})
export class CqlaunchPage extends CQBase implements OnInit {

  constructor(public router: Router, public dataservice: DataService, public uiService: CquiserviceService) {
    super(router, dataservice, uiService);
  }

  ngOnInit() {
  }

  ionViewWillEnter() {

    // this.uiService.presentLoading('Loading...').then(lc => {

    //   lc.present().then(() => {
    //     this.autologin();
    //     // lc.dismiss();
    //   }).catch(er => {
    //     lc.dismiss();
    //     console.log('Auto Login Error1:', er);
    //     this.navigate('login');
    //   });
    // }).catch(err => {
    //   console.log('Auto Login Error:', err);
    //   // this.router.navigateByUrl('/login');
    //   this.dismissLoading();
    //   this.navigate('login');
    // });

    this.autologin();
  }

  public autologin() {

    // Get data from storage
    this.dataservice.getFromCache(Utils.CURRENT_USER).then(credentials => {

      this.call('authenticate', credentials, success => {

        // Store current logged in user into object
        this.dataservice.current_login = new CQLogin(credentials.username, credentials.password);
        this.dataservice.current_user = success;
        
        this.dismissLoading();

        // Navigate to home page 
        this.rootnavigate('menu/home');

      }, fail => {

        this.dismissLoading();
        this.navigate('login');

      }, CQLogin.ADMIN_USER);
    }, error => {

      this.dismissLoading();
      this.rootnavigate('login');

    });
  }
}
