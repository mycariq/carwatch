import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CqlaunchPage } from './cqlaunch.page';

describe('CqlaunchPage', () => {
  let component: CqlaunchPage;
  let fixture: ComponentFixture<CqlaunchPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CqlaunchPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CqlaunchPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
