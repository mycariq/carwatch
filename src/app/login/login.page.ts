import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { CQBase } from '../toolkit/cqbase';
import { DataService } from '../service/data.service';
import { CQLogin } from '../toolkit/cqlogin';
import { IonInput, ModalController } from '@ionic/angular';
import { Utils } from '../helper/utils';
import { CquiserviceService } from '../service/cquiservice.service';
import { TermsComponent } from './terms/terms.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage extends CQBase implements OnInit {

  @ViewChild('mobileNo') mobileNo: IonInput;
  mobileNumber: String;
  iagree: boolean = false;

  constructor(public router: Router, public dataservice: DataService, public uiService: CquiserviceService, public modalController: ModalController) {
    super(router, dataservice, uiService);
  }

  ngOnInit() {
  }

  /**
   * termsAndConditions
   */
  public async termsAndConditions() {
    // alert("terms and conditions");
    const modal: HTMLIonModalElement =
      await this.modalController.create({
        component: TermsComponent
      });

    await modal.present();
  }

  /**
   * login
   */
  public login() {

    if (this.mobileNumber === undefined || this.mobileNumber === null || this.mobileNumber.length === 0) {
      this.presentAlertMessage('Alert', 'Please enter mobile number').then(() => {
        this.mobileNo.setFocus();
      });
      return;
    }

    if (!this.iagree) {
      this.presentAlertMessage('Alert', 'Please accept terms and conditions to continue');
      return;
    }

    let requestJson = {
      userName: this.mobileNumber
    };

    // Call login api
    this.call('loginrequest', requestJson,
      sucess => {

        this.showSuccessMessage(sucess, 'OTP Sent Successfully!');

        // Save user credentials in native storage 
        this.dataservice.saveIntoCache(Utils.USERNAME, this.mobileNumber);
        this.navigate('verification');
      }, failture => {
        this.showErrorMessage(failture, 'Please enter valid mobile number!');
      },
      CQLogin.NEW_USER);
  }

}
