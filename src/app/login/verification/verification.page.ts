import { Component, OnInit } from '@angular/core';
import { CQBase } from 'src/app/toolkit/cqbase';
import { Router } from '@angular/router';
import { DataService } from 'src/app/service/data.service';
import { CQLogin } from 'src/app/toolkit/cqlogin';

import { Utils } from 'src/app/helper/utils';
import { CquiserviceService } from 'src/app/service/cquiservice.service';

@Component({
  selector: 'app-verification',
  templateUrl: './verification.page.html',
  styleUrls: ['./verification.page.scss'],
})
export class VerificationPage extends CQBase implements OnInit {

  otp1: string = null;
  otp2: string = null;
  otp3: string = null;
  otp4: string = null;

  constructor(public router: Router, public dataservice: DataService, public uiService: CquiserviceService) {
    super(router, dataservice, uiService);
  }

  ngOnInit() {
  }

  /**
   * verifyOTP
   */
  public verifyOTP() {
    if (this.otp1 === null || this.otp2 === null || this.otp3 === null || this.otp4 === null) {
      this.presentAlertMessage('Alert', 'Please enter OTP');
      // alert('Please enter OTP');
      return;
    }

    const otp: string = this.otp1.toString() + this.otp2.toString() + this.otp3.toString() + this.otp4.toString();

    if (otp.trim().length < 4) {
      this.presentAlertMessage('Alert', 'Please enter Valid OTP');
      // alert('Please enter OTP');
      return;
    }

    this.dataservice.getFromCache(Utils.USERNAME).then(data => {

      let request = {
        userName: data,
        otp: otp
      };
      this.call('loginprocess', request, credentials => {

        let authRequest = {
          username: credentials.userName,
          password: credentials.password
        }

        // Call authenticate observer api
        this.call('authenticate', authRequest, success => {
          
          // Set login credentials of current user into native storage.
          this.dataservice.saveIntoCache(Utils.CURRENT_USER, authRequest);

          // Store current logged in user into object
          this.dataservice.current_login = new CQLogin(authRequest.username, authRequest.password);
          this.dataservice.current_user = success;
          
          // Navigate to home page
          this.rootnavigate('menu/home');

        }, fail => {
          this.showErrorMessage(fail, 'Failed!!');
        }, CQLogin.ADMIN_USER);

      }, fail => {
        this.showErrorMessage(fail, 'Please enter valid OTP');
      }, CQLogin.NEW_USER);

    });
  }

  /**
   * resendOTP
   */
  public resendOTP() {

    // Get data from storage
    this.dataservice.getFromCache(Utils.USERNAME).then(username => {
      let request = {
        userName: username
      }
      this.call('loginrequest', request, success => {
        alert('Otp sent successfully');
      }, fail => {
        alert('OTP Resent Failed!!')
      })
    });
  }

  // OTP box set next
  public next(src, dest) {
    if (src.value.length > 0) {
      dest.setFocus();
    }
  }

}
