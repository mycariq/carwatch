export class Vehicle {
    id: any;
    registrationNumber: String;
    latitude: any;
    longitude: any;
    locationShareUrl: String;
}
