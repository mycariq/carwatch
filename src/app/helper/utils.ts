// import * as moment from 'moment';
// import { CQSettings } from '../cqsettings.js';
import { config } from '../appconfig.js';
// import swal from 'sweetalert2';

export class Utils {

    // --------------------- constants ---------------------------------

    public static readonly DATE_FMT = 'yyyy-MM-dd';
    public static readonly DATE_TIME_FMT = `${Utils.DATE_FMT} hh:mm:ss`;
    public static readonly DATE_TIME = `YYYY-MM-DD hh:mm:ss`;
    public static readonly VEHICLE_MARKER = {
        CAR: 'assets/imgs/car_marker.png',
        BIKE: 'assets/imgs/bike_marker.png',
        BUS: 'assets/imgs/bus_marker.png',
        TRUCK: 'assets/imgs/truck_marker.png'
    }

    public static readonly VEHICLE_ICON = {
        CAR: 'assets/imgs/car.svg',
        BIKE: 'assets/imgs/bike.svg',
        BUS: 'assets/imgs/bus.svg',
        TRUCK: 'assets/imgs/truck.svg'
    }

    public static readonly USERNAME = 'username';
    public static readonly CURRENT_USER = 'CURRENT_USER';
    public static readonly DEFAULT_VEHICLE_MARKER = 'assets/imgs/marker.png';

    // --------------------- static functions---------------------------------

    /**
     * generateAuthToken
        private username: String,
        private md5password: string
    */
    public static generateAuthToken(username: String, md5password: String): String {
        return btoa(username + ':' + md5password);
    }


    /**
     * name
     */
    public static createUrl(rawUrl: String, inputJson: any): String {
        // Create api url
        let url = rawUrl;

        // TODO : Collect all input keys from url to replace with its values from inputJSON
        let inputKeys = [];

        for (const key in inputJson) {
            // replace key with its value in given url and make new url
            url = url.replace(new RegExp('{' + key + '}', 'gi'), inputJson[key]);
        }
        return url;
    }


    /**
    * name
    */
    public static createRequestJSON(apiRequestJSON: any, inputJSON: any): any {

        if (apiRequestJSON.fileUpload === true) {
            return inputJSON['file'];
        }

        const outputJSON: any = {};
        // To GET call, set default empty list
        if (apiRequestJSON.method === 'GET') {
            apiRequestJSON.params = [];
        }
        // Read input params json
        for (const key in inputJSON) {
            if (apiRequestJSON.params.includes(key)) {
                outputJSON[key] = inputJSON[key];
            }
        }
        // Final json
        return outputJSON;
    }

    public static strToObj(input: any, outPutClass: any): any {
        let inputata = JSON.parse(input);
        let outputObj = Object.setPrototypeOf(inputata, outPutClass.prototype)
        console.log(outputObj);
        return outputObj;
    }

    /**
     * setMarker
     */
    public static setMarker(type: String) {
        if (type)
            return Utils.VEHICLE_MARKER[type.toUpperCase()];
        else
            return Utils.DEFAULT_VEHICLE_MARKER
    }

    /**
     * setIcon
     */
    public static setIcon(type: String) {
        if (type)
            return Utils.VEHICLE_ICON[type.toUpperCase()];
        else
            return Utils.VEHICLE_ICON['CAR'];
    }
}
