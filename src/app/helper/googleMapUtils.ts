import { GoogleMap, LatLngBounds, LatLng, CameraPosition, ILatLngBounds } from '@ionic-native/google-maps/ngx';

export class googleMapUtils {
    static animateCameraWithBounds(map: GoogleMap, latlngbnds: any) {
 
        const ltBounds = new LatLngBounds(latlngbnds);

        ltBounds.extend(new LatLng(ltBounds.northeast.lat + 0.005, ltBounds.northeast.lng + 0.005));
        ltBounds.extend(new LatLng(ltBounds.southwest.lat - 0.005, ltBounds.southwest.lng - 0.005));

        const position: CameraPosition<ILatLngBounds> = {
            target: ltBounds,
            tilt: 0,
            duration: 1000
        };
        map.animateCamera(position);
    }
}