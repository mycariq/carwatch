import { Component, OnInit } from '@angular/core';
import { CQBase } from '../toolkit/cqbase';
import { CquiserviceService } from '../service/cquiservice.service';
import { DataService } from '../service/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage extends CQBase implements OnInit {

  name: String;

  pages = [
    {
      title: 'Home',
      url: '/menu/home',
      icon: 'home'
    },
    {
      title: 'Watch List',
      url: '/menu/vehicles',
      icon: 'car'
    }
  ]

  constructor(public router: Router, public dataservice: DataService, public uiService: CquiserviceService) {
    super(router, dataservice, uiService);
  }

  ngOnInit() {

  }

  ionViewWillEnter(){
    this.name = this.dataservice.current_user.name;
  }

  /**
   * logout
   */
  public logout() {
    this.dataservice.clearCache();
  }
}
