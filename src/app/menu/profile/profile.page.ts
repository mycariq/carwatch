import { Component, OnInit, ViewChild } from '@angular/core';
import { CQBase } from 'src/app/toolkit/cqbase';
import { Router } from '@angular/router';
import { DataService } from 'src/app/service/data.service';
import { CquiserviceService } from 'src/app/service/cquiservice.service';
import { User } from 'src/app/interface/user';
import { IonInput } from '@ionic/angular';
import { Location } from '@angular/common';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage extends CQBase implements OnInit {

  @ViewChild('txtName') txtName: IonInput;
  @ViewChild('txtemail') txtemail: IonInput;
  @ViewChild('txtmobile') txtmobile: IonInput;

  user: User = {
    name: '',
    email: '',
    cellNumber: ''
  };

  constructor(public router: Router, public dataservice: DataService, public uiService: CquiserviceService, public location: Location) {
    super(router, dataservice, uiService);
  }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.user = this.dataservice.current_user;
  }

  /**
   * updateProfile
   */
  public updateProfile() {

    if (this.user.name === null || this.user.name.length === 0) {
      this.presentAlertMessage('Alert', 'Please enter name');
      this.txtName.setFocus();
      return;
    }

    // if (this.user.email === null || this.user.email.length === 0) {
    //   this.presentAlertMessage('Alert', 'Please enter email');
    //   this.txtemail.setFocus();
    //   return;
    // }


    // Update user profile
    this.call('updateProfile', this.user, success => {

      this.showSuccessMessage(success, "Profile updated successfully");

      // Store updated info
      this.dataservice.current_user = this.user;

      this.location.back();

    }, fail => {
      this.showErrorMessage(fail, 'Profile updation failed!');
    });
  }
}
