import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from 'src/app/service/data.service';
import { CquiserviceService } from 'src/app/service/cquiservice.service';
import { HomePage } from 'src/app/home/home.page';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import * as moment from 'moment';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { Utils } from 'src/app/helper/utils';

@Component({
  selector: 'app-vehicles',
  templateUrl: './vehicles.page.html',
  styleUrls: ['./vehicles.page.scss'],
})
export class VehiclesPage extends HomePage implements OnInit {

  constructor(public router: Router, public dataservice: DataService, public uiService: CquiserviceService, 
    public inappbrowser: InAppBrowser, public callNumber: CallNumber, public keyboard: Keyboard) {
    super(router, dataservice, uiService, inappbrowser, callNumber, keyboard);
  }

  ngOnInit() {

    //Call get Obserable vehicles of given observer
    this.getVehicles();
  }

  /**
   * getVehicles
   */
  public getVehicles() {
    //Call get Obserable vehicles of given observer
    this.call('getObservation', {}, response => {

      // Return only active vehicles
      this.vehicles = response.filter(row => {
        row.setIcon = Utils.setIcon(row.carDetail.vehicleType)
        return moment(row.observeTill).isAfter(moment().valueOf());
      });

    }, fail => {
      this.presentAlertMessage('Alert', 'No vehicles to track!!');
    });

  }

}
