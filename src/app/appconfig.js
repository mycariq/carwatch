export const config = {
    host: "dev-ecu.mycariq.com",  // @TMPL host: "[% ROOT.servers.api.endPoint %]", @TMPL
    port: "443", // @TMPL port: "[% ROOT.servers.api.port %]", @TMPL
    appName: "Cariq" // @TMPL appName: "[% ROOT.servers.fleetiqwebapp.appName %]" @TMPL
}
