import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { CquiserviceService } from './service/cquiservice.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

  // set up hardware back button event.
  lastTimeBackPress = 0;
  timePeriodToExit = 1000;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public uiService: CquiserviceService,
    public router: Router
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleLightContent();
      this.splashScreen.hide();

      this.platform.backButton.subscribe(() => {
        console.log('Router Info:', this.router);
        if (this.router.url === '/menu/home' || this.router.url === '/login') {

          //Exit app
          navigator['app'].exitApp();


          // if (new Date().getTime() - this.lastTimeBackPress < this.timePeriodToExit) {
          //   // tslint:disable-next-line:no-string-literal
          //   navigator['app'].exitApp();

          // } else {
          //   this.uiService.presentToastMessage('Press back again to exit App.', 1000);
          //   this.lastTimeBackPress = new Date().getTime();
          // }

        } else {
          // go back
          console.log('go back');
        }
      });

    });
  }
}
