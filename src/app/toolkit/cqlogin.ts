import { Utils } from '../helper/utils';

export class CQLogin {

    username: String;
    password: String;

    constructor(username: String, password: String) {
        this.username = username;
        this.password = password;
    }

    public static NO_AUTH = new CQLogin('', '');
    public static NEW_USER = new CQLogin('heimdall', '502f8bc03ebccab357517e46464af8b3');
    public static ADMIN_USER = new CQLogin('cariqadmin', '5453f828fb413175ed2f0e991a8c07be');

    /**
     * authToken
     */
    public authToken() {
        return 'Basic ' + Utils.generateAuthToken(this.username, this.password);
    }

}
