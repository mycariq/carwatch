import { CQLogin } from './cqlogin';
import { CQServer } from './cqserver';
import { HttpHeaders } from '@angular/common/http';
import { Utils } from '../helper/utils';

export class CQAPI {

    login: CQLogin;
    server: CQServer;
    inputJSON: any;
    apiRequestJSON: any;

    constructor(apiJSON: any, login: CQLogin, server: CQServer, inputJSON: any) {
        this.login = login;
        this.server = server;
        this.inputJSON = inputJSON;
        this.apiRequestJSON = apiJSON;
    }

    /**
     * method
     */
    public method(): string {
        return this.apiRequestJSON.method;
    }

    /**
     * url
     */
    public url(): string {
        let rawUrl = this.server.baseUrl() + this.apiRequestJSON.url;
        return Utils.createUrl(rawUrl, this.inputJSON).toString();
    }

    /**
     * inputJson
     */
    public inputParams(): any {
        // Make url based on input param
        return Utils.createRequestJSON(this.apiRequestJSON, this.inputJSON);
    }

    /**
     * headers
     */
    public headers(): any {
        // Create http header
        let headers = new HttpHeaders();
        // Set basic auth token
        if(this.server.host == "https://google.com"){
          headers.append("Access-Control-Allow-Origin", "*");
          // alert("google api called inside the cqapi");
          return headers;
        }
        headers = headers.append('Authorization', this.login.authToken());
        // Check for the file upload reuqest
        if (this.apiRequestJSON.fileUpload !== true)
            headers = headers.append('Content-Type', 'application/json');

        return headers;
    }
}
