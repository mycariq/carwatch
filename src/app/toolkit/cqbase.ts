import { Router } from '@angular/router';
import { DataService } from '../service/data.service';
import { CQLogin } from './cqlogin';
import { CQServer } from './cqserver';
import { CquiserviceService } from '../service/cquiservice.service';

export class CQBase {

    constructor(protected router: Router, protected dataservice: DataService, protected uiService: CquiserviceService) {
    }

    public navigate(path: String) {
        this.router.navigate([path]);
    }


    public rootnavigate(path: string) {
        this.router.navigate([path], { replaceUrl: true });
    }
    /**
   * call
   */
    public async call(apiKey: string, convertor: any, success?: any, fail?: any, login?: CQLogin, server?: CQServer) {

        // await this.uiService.presentLoading('Loading...').then(lc => {
        //     lc.present().then(() => {
        //         // Call api
        //         this.dataservice.call(apiKey, convertor, data => {
        //             success(data);
        //             lc.dismiss();
        //         }, err => {
        //             lc.dismiss();
        //             console.log('Error:', err);
        //             fail(err);
        //         }, login, server);

        //     }).catch(cerr => {
        //         lc.dismiss();
        //         console.log('Error:', cerr);
        //         fail(cerr);
        //     }).finally(() => {
        //         // lc.dismiss();
        //     });
        // });

        this.dataservice.call(apiKey, convertor, data => {
            success(data);
        }, err => {
            fail(err);
        }, login, server);

    }


    // UI presentaing Loader, Toast aand Alert
    async presentAlertMessage(header: string, message: string, subheader: string = null) {
        return await this.uiService.presentAlertMessage(header, message, subheader);
    }

    async presentAlertMessageMultipleButtons(header: string, message: string, subheader: string = null, buttons: any) {
        return await this.uiService.presentAlertMessageMuiltipleButtons(header, message, subheader, buttons);
    }

    async presentToastMessage(message: string, duration: number = 2000) {
        return await this.uiService.presentToastMessage(message, duration);
    }

    async presentTransparentToastMessage() {
        return await this.uiService.presentTransparentToastMessage();
    }

    async presentLoading(message: string = 'Loading...') {
        return await this.uiService.presentLoading(message);
    }

    async dismissLoading() {
        return await this.uiService.dismissLoading();
    }

    showErrorMessage(error: any, message: string) {
        this.presentAlertMessage('Error', (error.error.messages === undefined) ? message : error.error.messages[0]);
    }

    showSuccessMessage(success: any, message: string) {
        // this.presentAlertMessage('Success', (success.messages === undefined) ? message : success.messages[0]);
        this.presentToastMessage((success.messages === undefined) ? message : success.messages[0]);
    }


    navigateControl(event, next, prev) {
        if (event.target.value.length < 1 && prev) {
            prev.setFocus();
        } else if (next && event.target.value.length > 0) {
            next.setFocus();
        } else {
            return;
        }
    }
}