import { config } from '../appconfig.js';

export class CQServer {

    host: String;
    port: Number;

    constructor(host: String, port?: Number) {
        this.host = host;
        this.port = port;
    }

    public static DEFAULT = new CQServer(config.host, config.port);

    public static GOOGLE = new CQServer('https://google.com');

    /**
     * baseUrl
     */
    public baseUrl() {
        if (this.port === undefined)
            return this.host + '/';
        return "https://" + this.host + ':' + this.port + '/';
    }
}
